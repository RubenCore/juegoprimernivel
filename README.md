# Super Trozo De Carne #

# Controles

    ~ a -> movimiento hacia la izquierda
    ~ d -> movimiento hacia la derecha
    ~ w -> salto
    ~ u -> subir escaleras/cuerdas
    ~ j -> bajar escaleras/cuerdas
    ~ Atajos -> Desde el menu pulsar la p para ir al primer nivel, pulsar la o para ir al segundo nivel, 
                para volver al nivel menu pulsar l (sirve en ambos niveles)
    
# Requisitos minimos para la segunda entrega

    ~ Clases Abstracta
    
        -He utilizado una clase padre abstracta llamada Enemigo, la cual tiene dos hijos DinoNaranja y DinoVerde, 
         las cuales heredan las funciones del padre.
        
    ~ Clases Singleton
        
        -En tema de aplicar las clases Singleton, he cambiado el PJ a Singleton y he creado una clase WindowSingleton para hacer el cambio de nivel.
    
    ~ Clases Interficie
    
        -La Interficie que he creado se llama escalable, esta interfaz se implementa tanto en las escaleras del nivel 1 y las cuerdas del nivel 2, 
         lo que permite esta interficie es una mejora a la hora de subir y bajar las escaleras o las cuerdas.
    
    ~ Cambio de nivel
        
        - He creado un nuevo nivel en el cual implemento los dos enemigos y las cuerdas, el cambio de nivel lo hago con el window singleton, 
          al tocar las respectivas puertas pues moverte a traves de los niveles.

# Mejoras adicionales 

    He implementado una mejora en los pinchos que se esconden en el nivel 1, la clase se entiende mejor y mejora un poco mas la eficiencia del juego.
    
    Al implementar la interficie escalable, se ha mejorado drasticamente el subir y bajar por las escaleras y las cuerdas.
    
    He añadido una funcion cleanUP en cada main de cada nivel, esta funcion lo que hace es limpiar las listas, 
    he creado esta funcion para que quede mas ordenado el codigo.