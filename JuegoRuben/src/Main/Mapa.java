package Main;

import Core.Field;
import Core.Sprite;
import Core.Window;

import java.util.ArrayList;

import org.w3c.dom.Text;

public class Mapa extends Sprite {

	public Mapa(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
		terrain = true;
	}

	public static ArrayList<Sprite> nivel1 = new ArrayList<>();
	public static ArrayList<Sprite> sp = new ArrayList<>();
	public static ArrayList<Sprite> plataforma = new ArrayList<>();
	public static ArrayList<Sprite> pinchos = new ArrayList<>();
	public static ArrayList<Sprite> pinchosSube = new ArrayList<>();
	public static ArrayList<Sprite> pinchosM = new ArrayList<>();
	public static ArrayList<Sprite> trampas = new ArrayList<>();
	public static ArrayList<Sprite> puertasNivel1 = new ArrayList<>();

	/**
	 * 
	 * PrimerNivel
	 * 
	 *  Crea crea todo el mapeado del nivel 1
	 * 
	 */

	public static void primerNivel(Field f) {
		Mapa suelo = new Mapa("suelo", 0, 1000, 1900, 1030, "IMG/testsuelo2.png");
		Mapa paredDER = new Mapa("ParedDER", 1890, 0, 1900, 1000, " ");
		Mapa paredIZQ = new Mapa("ParedIZQ", 0, 0, 10, 1000, " ");
		// Mapa techo= new Mapa("techo", 0, 0, 20, 1000, "escalera.png");
		Mapa plataforma1 = new Mapa("Plataforma 1", 150, 951, 350, 1001, "IMG/plataforma.png");
		Mapa plataforma2 = new Mapa("Plataforma 2", 425, 901, 625, 951, "IMG/plataforma.png");
		Mapa plataforma3 = new Mapa("Plataforma 2", 680, 851, 880, 901, "IMG/plataforma.png");

		Mapa plataforma4 = new Mapa("PlataformaEscalera1", 1640, 525, 1840, 575, "IMG/plataforma.png");
		Mapa plataforma5 = new Mapa("PlataformaEscalera2", 1440, 525, 1640, 575, "IMG/plataforma.png");
		Mapa plataforma6 = new Mapa("PlataformaEscalera3", 1240, 525, 1440, 575, "IMG/plataforma.png");
		Mapa plataforma7 = new Mapa("PlataformaEscalera4", 1040, 525, 1240, 575, "IMG/plataforma.png");
		Mapa plataforma8 = new Mapa("PlataformaEscalera5", 840, 525, 1040, 575, "IMG/plataforma.png");
		Mapa plataforma9 = new Mapa("PlataformaEscalera6", 641, 525, 841, 575, "IMG/plataforma.png");
		Mapa plataforma10 = new Mapa("PlataformaEscalera7", 441, 525, 641, 575, "IMG/plataforma.png");
		Mapa plataforma11 = new Mapa("PlataformaEscalera8", 241, 525, 441, 575, "IMG/plataforma.png");
		Mapa plataforma12 = new Mapa("PlataformaEscalera9", 41, 525, 241, 575, "IMG/plataforma.png");
		Mapa plataforma13 = new Mapa("PlataformaEscalera10", -10, 525, 40, 575, "IMG/bloqueMuro50x50.png");

		Mapa plataforma14 = new Mapa("PlataformaEscalera1", 1660, 275, 1860, 325, "IMG/plataforma.png");
		Mapa plataforma15 = new Mapa("PlataformaEscalera1", 1460, 275, 1660, 325, "IMG/plataforma.png");
		Mapa plataforma16 = new Mapa("PlataformaEscalera1", 1260, 275, 1460, 325, "IMG/plataforma.png");
		Mapa plataforma17 = new Mapa("PlataformaEscalera1", 1060, 275, 1260, 325, "IMG/plataforma.png");
		Mapa plataforma18 = new Mapa("PlataformaEscalera1", 860, 275, 1060, 325, "IMG/plataforma.png");
		Mapa plataforma19 = new Mapa("PlataformaEscalera1", 660, 275, 860, 325, "IMG/plataforma.png");
		Mapa plataforma20 = new Mapa("PlataformaEscalera1", 460, 275, 660, 325, "IMG/plataforma.png");
		Mapa plataforma21 = new Mapa("PlataformaEscalera1", 260, 275, 460, 325, "IMG/plataforma.png");
		Mapa plataforma22 = new Mapa("PlataformaEscalera9", 60, 275, 260, 325, "IMG/plataforma.png");
		Mapa plataforma23 = new Mapa("PlataformaEscalera10", 1860, 275, 1910, 325, "IMG/bloqueMuro50x50.png");
		Mapa plataforma24 = new Mapa("PlataformaEscalera10", 1810, 225, 1860, 275, "IMG/bloqueMuro50x50.png");
		Mapa plataforma25 = new Mapa("PlataformaEscalera10", 1860, 225, 1910, 275, "IMG/bloqueMuro50x50.png");
		Mapa plataforma26 = new Mapa("PlataformaEscalera10", 1611, 226, 1811, 276, "IMG/plataforma.png");
		Mapa plataforma27 = new Mapa("PlataformaEscalera10", 1711, 176, 1911, 226, "IMG/plataforma.png");
		Mapa plataforma28 = new Mapa("PlataformaEscalera10", 1811, 126, 1861, 176, "IMG/bloqueMuro50x50.png");
		Mapa plataforma29 = new Mapa("PlataformaEscalera10", 1861, 126, 1911, 176, "IMG/bloqueMuro50x50.png");

		Mapa bloque1 = new Mapa("Bloque1", 1555, 880, 1605, 930, "IMG/bloqueMuro50x50.png");
		Mapa bloque2 = new Mapa("Bloque2", 1605, 880, 1655, 930, "IMG/bloqueMuro50x50.png");
		Mapa bloque3 = new Mapa("Bloque3", 1720, 840, 1770, 890, "IMG/bloqueMuro50x50.png");

		Mapa bloque5 = new Mapa("Bloque5", 1840, 750, 1890, 800, "IMG/bloqueMuro50x50.png");
		Mapa bloque6 = new Mapa("Bloque2", 1655, 880, 1705, 930, "IMG/bloqueMuro50x50.png");
		Mapa bloque7 = new Mapa("Bloque2", 310, 225, 360, 275, "IMG/bloqueMuro50x50.png");

		Mapa muro = new Mapa("Muro", 1005, 840, 1555, 1000, "IMG/muro.png");

		Mapa pinchos1 = new Mapa("Pinchos", 1555, 865, 1655, 885, "IMG/pinchos.png");
		Mapa pinchos2 = new Mapa("Pinchos", 1555, 950, 1900, 1000, "IMG/pinchos2.png");
		Mapa pinchos3 = new Mapa("Pinchos", 1655, 865, 1705, 882, "IMG/pinchos50x25.png");

		Puerta puerta = new Puerta("PuertaNvl1", 1830, 25, 1895, 125, "IMG/puerta.png");

		
		
		puertasNivel1.add(puerta);

		sp.add(suelo);
		sp.add(paredDER);
		sp.add(paredIZQ);
		plataforma.add(muro);
		plataforma.add(bloque1);
		plataforma.add(bloque2);
		plataforma.add(bloque6);
		plataforma.add(bloque3);	
		plataforma.add(bloque5);

		plataforma.add(plataforma1);
		plataforma.add(plataforma2);
		plataforma.add(plataforma3);

		plataforma.add(plataforma4);
		plataforma.add(plataforma5);
		plataforma.add(plataforma6);
		plataforma.add(plataforma7);
		plataforma.add(plataforma8);
		plataforma.add(plataforma9);
		plataforma.add(plataforma10);
		plataforma.add(plataforma11);
		plataforma.add(plataforma12);
		plataforma.add(plataforma13);
		plataforma.add(plataforma14);
		plataforma.add(plataforma15);
		plataforma.add(plataforma16);
		plataforma.add(plataforma17);
		plataforma.add(plataforma18);
		plataforma.add(plataforma19);
		plataforma.add(plataforma20);
		plataforma.add(plataforma21);
		plataforma.add(plataforma22);
		plataforma.add(plataforma23);
		plataforma.add(plataforma24);
		plataforma.add(plataforma25);
		plataforma.add(plataforma26);
		plataforma.add(plataforma27);
		plataforma.add(plataforma28);
		plataforma.add(plataforma29);
		plataforma.add(bloque7);

		pinchos.add(pinchos1);
		pinchos.add(pinchos3);
		pinchos.add(pinchos2);

		trampas.add(Nivel1_1.bloqueT);

		pinchosSube.addAll(Nivel1_1.pinchos);

		Nivel1_1.MB.x1 = 11;
		Nivel1_1.MB.y1 = 951;
		Nivel1_1.MB.x2 = 61;
		Nivel1_1.MB.y2 = 1001;

		sp.add(Nivel1_1.MB);

		nivel1.addAll(Items.getInstance(f).coinList);
		nivel1.addAll(sp);
		nivel1.addAll(pinchos);
		nivel1.addAll(pinchosM);
		nivel1.addAll(pinchosSube);
		nivel1.addAll(trampas);
		nivel1.addAll(puertasNivel1);
		nivel1.addAll(plataforma);
		nivel1.addAll(Escalera.escaleras);

	}

	public static ArrayList<Sprite> nivel0 = new ArrayList<>();
	public static ArrayList<Sprite> sp0 = new ArrayList<>();
	public static ArrayList<Sprite> puertaNivel0 = new ArrayList<>();
	public static ArrayList<Sprite> puertaNivel2 = new ArrayList<>();
	public static ArrayList<Sprite> puertaNivelMenuTienda = new ArrayList<>();

	/**
	 * 
	 * BaseNivel
	 * 
	 * Crea crea todo el mapeado del nivelMenu
	 * 
	 */
	public static void BaseNivel(Field f) {

		Main.MB.x1 = 125;
		Main.MB.y1 = 580;
		Main.MB.x2 = 175;
		Main.MB.y2 = 630;
		sp0.add(Main.MB);

		Mapa suelo = new Mapa("suelo", 0, 875, 1900, 1030, "IMG/testsuelo2.png");
		Puerta puerta = new Puerta("PuertaNvl1", 840, 750, 955, 874, "IMG/puertamenu.png");
		Puerta puerta2 = new Puerta("PuertaNvl2", 830, 338, 945, 462, "IMG/puertamenu.png");
		Mapa paredDER = new Mapa("ParedDER", 1890, 0, 1900, 1000, " ");
		Mapa paredIZQ = new Mapa("ParedIZQ", 0, 0, 60, 1000, " ");
		Mapa plat1 = new Mapa("plat1", 360, 720, 480, 780, "IMG/testsuelo2.png ");
		Mapa plat2 = new Mapa("plat2", 540, 645, 778, 686, "");
		Mapa plat3 = new Mapa("plat3", 1020, 600, 1380, 875, "");
		Mapa plat4 = new Mapa("plat4", 840, 690, 1020, 730, "");
		Mapa plat5 = new Mapa("plat5", 970, 650, 1020, 700, "IMG/testsuelo2.png ");
		Mapa plat6 = new Mapa("plat6", 1080, 540, 1140, 600, "IMG/testsuelo2.png ");
		Mapa plat7 = new Mapa("plat7", 780, 460, 1017, 500, "");
		Mapa suelo1 = new Mapa("suelo1", 0, 830, 770, 870, "");
		Mapa suelo2 = new Mapa("suelo2", 60, 785, 650, 827, "");
		Mapa suelo3 = new Mapa("suelo3", 60, 645, 175, 780, "");
		Mapa suelo4 = new Mapa("suelo4", 60, 370, 117, 641, "");
		Mapa suelo5 = new Mapa("suelo1", 1380, 735, 1860, 875, "");
		Mapa suelo6 = new Mapa("suelo1", 1680, 560, 1866, 755, "");
		
		Puerta puerta3 = new Puerta("PuertaMenuTienda", 1300, 450, 1370, 600, "IMG/puertaTienda.png");		
		
		sp0.add(plat1);
		sp0.add(plat2);
		sp0.add(plat3);
		sp0.add(plat4);
		sp0.add(plat5);
		sp0.add(plat6);
		sp0.add(plat7);
		sp0.add(suelo6);
		sp0.add(suelo5);
		sp0.add(suelo4);
		sp0.add(suelo3);
		sp0.add(suelo1);
		sp0.add(suelo2);
		sp0.add(suelo);
		sp0.add(paredIZQ);
		sp0.add(paredDER);
		puertaNivel0.add(puerta);
		puertaNivel2.add(puerta2);
		puertaNivelMenuTienda.add(puerta3);

		nivel0.addAll(puertaNivel2);
		nivel0.addAll(puertaNivelMenuTienda);
		nivel0.addAll(puertaNivel0);
		nivel0.addAll(sp0);

	}

	public static ArrayList<Sprite> sp2 = new ArrayList<>();
	public static ArrayList<Sprite> nivel2 = new ArrayList<>();
	public static ArrayList<Sprite> platLvL2 = new ArrayList<>();
	public static ArrayList<Sprite> pinchosLvL2 = new ArrayList<>();
	public static ArrayList<Sprite> bloquesIzq = new ArrayList<>();
	public static ArrayList<Sprite> bloquesDer = new ArrayList<>();
	public static ArrayList<Sprite> bloqueSalto = new ArrayList<>();
	public static ArrayList<Sprite> puertaNivel2Menu = new ArrayList<>();

	/**
	 * 
	 * PrimerNivel
	 * 
	 *  Crea crea todo el mapeado del nivel 2
	 * 
	 */
	public static void SegundoNivel(Field f) {

		Nivel1_2.MB.x1 = 1845;
		Nivel1_2.MB.y1 = 60;
		Nivel1_2.MB.x2 = 1895;
		Nivel1_2.MB.y2 = 110;
		sp2.add(Nivel1_2.MB);

		Mapa suelo = new Mapa("suelo", 0, 1000, 1920, 1030, "IMG/testsuelo2.png");
		sp2.add(suelo);
		Mapa paredDER = new Mapa("ParedDER", 1890, 0, 1900, 1000, " ");
		Mapa paredIZQ = new Mapa("ParedIZQ", 0, 0, 10, 1000, " ");
		sp2.add(paredDER);
		sp2.add(paredIZQ);
		Mapa plataformaPiso1 = new Mapa("plataformaPiso1", 0, 600, 1815, 640, "IMG/PlatLvL2.jpg");
		Mapa plataformaSpawn = new Mapa("plataformaSpawn", 1830, 110, 1900, 150, "IMG/bloqueLvL2.png");
		Mapa plataforma2_1 = new Mapa(" plataforma2_1", 1740, 150, 1830, 190, "IMG/bloqueLvL2.png");
		Mapa plataforma2_2 = new Mapa(" plataforma2_2", 1610, 185, 1740, 225, "IMG/bloqueLvL2.png");
		Mapa plataforma2_3 = new Mapa(" plataforma2_3", 1520, 150, 1610, 190, "IMG/bloqueLvL2.png");
		Mapa plataforma2_4 = new Mapa(" plataforma2_4", 90, 215, 1520, 255, "IMG/PlatLvL2.jpg");
		Mapa plataforma2_5 = new Mapa(" plataforma2_5", 1520, 560, 1590, 600, "IMG/bloqueLvL2.png");
		Mapa plataforma2_6 = new Mapa(" plataforma2_6", 340, 560, 410, 600, "IMG/bloqueLvL2.png");

		Mapa plataforma2_7 = new Mapa(" plataforma2_7", 1500, 960, 1570, 1000, "IMG/bloqueLvL2.png");
		Mapa plataforma2_8 = new Mapa(" plataforma2_8", 330, 960, 400, 1000, "IMG/bloqueLvL2.png");

		Mapa techo2_1 = new Mapa("techo2_1", 450, 0, 1280, 40, "IMG/PlatLvL2.jpg");

		Mapa pinchos2_1 = new Mapa("pinchos2_1", 1610, 170, 1740, 195, "IMG/pinchos.png");
		Mapa pinchos2_2 = new Mapa("pinchos2_2", 1290, 193+8, 1340, 218, "IMG/pinchos50x25.png");
		Mapa pinchos2_3 = new Mapa("pinchos2_3", 1060, 193+8, 1110, 218, "IMG/pinchos50x25.png");
		Mapa pinchos2_4 = new Mapa("pinchos2_4", 840, 193+8, 890, 218, "IMG/pinchos50x25.png");
		Mapa pinchos2_5 = new Mapa("pinchos2_5", 620, 193+8, 670, 218, "IMG/pinchos50x25.png");
		Mapa pinchosTecho2_1 = new Mapa("pinchosTecho2_1", 1130, 33-6, 1260-4, 63-10, "IMG/pinchosTecho.png");
		Mapa pinchosTecho2_2 = new Mapa("pinchosTecho2_2", 900, 33-6, 1030-4, 63-10, "IMG/pinchosTecho.png");
		Mapa pinchosTecho2_3 = new Mapa("pinchosTecho2_3", 690, 33-6, 820-4, 63-10, "IMG/pinchosTecho.png");
		Mapa pinchosTecho2_4 = new Mapa("pinchosTecho2_4", 475, 33-6, 605-4, 63-10, "IMG/pinchosTecho.png");

		Mapa soporteCuerda2_1 = new Mapa("base", 0, 580, 100, 600, "IMG/base1.png");
		Mapa soporteCuerda2_2 = new Mapa("base2", 0, 25, 25, 590, "IMG/base2.png");
		Mapa soporteCuerda2_3 = new Mapa("base3", 0, 20, 70, 45, "IMG/base3.png");

		Mapa soporteCuerda2_2_1 = new Mapa("base2", 1820, 980, 1920, 1000, "IMG/base1.png");
		Mapa soporteCuerda2_2_2 = new Mapa("base2", 1885, 310, 1910, 985, "IMG/base2.png");
		Mapa soporteCuerda2_2_3 = new Mapa("base3", 1810, 300, 1910, 325, "IMG/base3.png");

		Mapa bloquenovisible = new Mapa("bloquedesalto", 910, 980, 912, 1000, "");
		bloquenovisible.terrain = false;
		
		Puerta puerta = new Puerta("PuertaNv2Menu", 0, 850, 60, 1000, "IMG/puertaL.png");

		puertaNivel2Menu.add(puerta);

		bloqueSalto.add(bloquenovisible);

		platLvL2.add(plataformaPiso1);
		platLvL2.add(plataformaSpawn);
		platLvL2.add(plataforma2_2);
		platLvL2.add(plataforma2_1);
		platLvL2.add(plataforma2_3);
		platLvL2.add(plataforma2_4);

		bloquesDer.add(plataforma2_5);
		bloquesIzq.add(plataforma2_6);

		bloquesDer.add(plataforma2_7);
		bloquesIzq.add(plataforma2_8);

		platLvL2.add(techo2_1);

		pinchosLvL2.add(pinchos2_1);
		pinchosLvL2.add(pinchos2_2);
		pinchosLvL2.add(pinchos2_3);
		pinchosLvL2.add(pinchos2_4);
		pinchosLvL2.add(pinchos2_5);
		pinchosLvL2.add(pinchosTecho2_1);
		pinchosLvL2.add(pinchosTecho2_2);
		pinchosLvL2.add(pinchosTecho2_3);
		pinchosLvL2.add(pinchosTecho2_4);

		platLvL2.add(soporteCuerda2_2);
		platLvL2.add(soporteCuerda2_1);
		platLvL2.add(soporteCuerda2_3);

		platLvL2.add(soporteCuerda2_2_2);
		platLvL2.add(soporteCuerda2_2_1);
		platLvL2.add(soporteCuerda2_2_3);

		nivel2.addAll(bloquesDer);
		nivel2.addAll(bloquesIzq);

		DinoNaranja dinoInstance = new DinoNaranja(Nivel1_2.Dino1);
		DinoVerde dinovInstance = new DinoVerde(Nivel1_2.Dino2);
		Nivel1_2.Dinos.add(dinoInstance);
		Nivel1_2.Dinos.add(dinovInstance);
		sp2.add(dinoInstance);
		sp2.add(dinovInstance);
		nivel2.addAll(bloqueSalto);
		nivel2.addAll(Cuerda.cuerdas);
		nivel2.addAll(sp2);
		nivel2.addAll(pinchosLvL2);
		nivel2.addAll(platLvL2);
		nivel2.addAll(puertaNivel2Menu);

	}
	
	
	
	public static ArrayList<Sprite> nivelTienda = new ArrayList<>();
	public static ArrayList<Sprite> spTienda = new ArrayList<>();
	public static ArrayList<Sprite> plataformaTienda = new ArrayList<>();
	public static ArrayList<Sprite> puertaNivelTienda = new ArrayList<>();
	
	
	public static void Tienda(Field f) {
		
		Tienda.MB.x1 = 130;
		Tienda.MB.y1 = 890;
		Tienda.MB.x2 = Tienda.MB.x1 + 50;
		Tienda.MB.y2 = Tienda.MB.y1 + 50;
		spTienda.add(Tienda.MB);

		Mapa sueloT = new Mapa("suelo", 0, 940, 1920, 1030, "");
		Mapa paredTizq = new Mapa("pared", 0, 0, 80, 1030, "");
		Mapa paredTder = new Mapa("pared", 1830, 0, 1920, 1030, "");
		Mapa plataforma1 = new Mapa("plataforma1", 220, 600, 520, 660, "IMG/base1.png");
		Mapa plataforma2 = new Mapa("plataforma2", 220+1200 , 600, 520+1200, 660, "IMG/base1.png");
		Mapa plataforma3 = new Mapa("plataforma3", 600 ,320, 900,380, "IMG/base1.png");
		
		
		Puerta puerta = new Puerta("PuertaNv2Menu", 625, 180, 695 , 320, "IMG/puertaL.png");
		
		puertaNivelTienda.add(puerta);
		
		plataformaTienda.add(plataforma3);
		plataformaTienda.add(plataforma2);
		plataformaTienda.add(sueloT);
		plataformaTienda.add(paredTizq);
		plataformaTienda.add(paredTder);
		plataformaTienda.add(plataforma1);
		
		
		
		plataformaTienda.add(Tienda.MB);
		
		nivelTienda.addAll(puertaNivelTienda);
		
		nivelTienda.addAll(Items.getInstance(f).itemList);
		
		
		nivelTienda.addAll(plataformaTienda);
		nivelTienda.addAll(spTienda);
		nivelTienda.addAll(Escalera.escaleras2);
	}
}
