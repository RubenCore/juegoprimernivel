package Main;

import java.util.ArrayList;

import Core.Field;
import Core.Sprite;

public class Items {
	
	private static Items instance = null;
	
	public static ArrayList<Item> itemList = new ArrayList<>();
	public static ArrayList<Item> coinList = new ArrayList<>();


	public Items(Field f) {
		Item itemVel = new Item("item1", 320, 470, 390, 540, "IMG/MasVelQuieto.png", ItemType.SPEED);
		Item itemjump = new Item("item1", 320+1200, 470, 390+1200, 540, "IMG/MasSalto.png", ItemType.JUMP);		
		Item coin = new Item("item1",875 ,410 ,875+50 ,410+50 , "IMG/moneda.gif", ItemType.COIN);
		

		itemList.add(itemVel);
		itemList.add(itemjump);
		coinList.add(coin);
	}
	
	public static Items getInstance(Field f) {
		if(instance == null) {
			instance = new Items(f);
		}
		return instance;
	}
	
}
