package Main;

import Core.Field;
import Core.Sprite;
import Core.Window;


public class PinchosSube extends Sprite {

	private int minY;
	private int maxY;
	boolean seMueve = false;
	boolean	sube = true;
	
	public PinchosSube(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
	}
	

	/**
	 * 
	 * SubidaPinchos 
	 * 
	 * Hace la subida y bajada de los pinchos 
	 * 
	 * Tambien mira la colision de los pinchos ocn el PJ
	 * 
	 */
	public void subidaPinchos() {
		if(seMueve) {
			if(sube) {
				y1-=2;
				y2-=2;
				if(y1<=minY) {
					sube = !sube;
				}
				
			}else {
				y1+=2;
				y2+=2;
				if(y2>=maxY) {
					sube = !sube;
				}
			}
		}
		if (this.collidesWith(Nivel1_1.MB)) {
			Nivel1_1.MB.x1 = 11;
			Nivel1_1.MB.y1 = 951;
			Nivel1_1.MB.x2 = 61;
			Nivel1_1.MB.y2 = 1001;
		}
	}

	/**
	 * statusInicial
	 * 
	 * inicializa los pichos en una posicion y determina hacia donde y cuanto se mueve
	 * 
	 * @param stat - si empieza hacia arriba o hacia abajo
	 * @param move - cantidad que se mueve
	 * @param x1 - posicion
	 * @param y1 - posicion
	 */
	public void statusInicial(boolean stat, int move, int x1, int y1) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x1+50;
		this.y2 = y1+25;
		this.sube=stat;
		this.minY=y1+((y2-y1)/2)-move;
		this.maxY=y1+((y2-y1)/2)+move;
		this.seMueve = true;
	}

	/**
	 * statusInicial
	 * 
	 * inicializa los pichos en una posicion, determina que los pinchos se mueven hacia arriba y cuanto se mueven
	 * 	
	 * @param move - cantidad que se mueve
	 * @param x1 - posicion
	 * @param y1 - posicion
	 */
	public void statusInicial(int move, int x1, int y1) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x1+50;
		this.y2 = y1+25;
		this.sube=true;
		this.minY=y1+((y2-y1)/2)-move;
		this.maxY=y1+((y2-y1)/2)+move;
		this.seMueve = true;
	}
}
