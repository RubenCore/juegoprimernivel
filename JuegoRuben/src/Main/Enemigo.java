package Main;

import Core.Sprite;
import Core.Window;

public abstract class Enemigo extends Sprite {

	int moveSpeed = 5;
	public Enemigo(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2,path);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * movimiento
	 * 
	 * funcion para crear los movimientos, en este caso los movimientos son los mismo para los dos enemigos
	 * 
	 */
	public abstract void movimiento();
	
	/**
	 * colision
	 * 
	 * implementara las colisiones de cada enemigo, en este caso son diferentes cada colision
	 * 
	 */
	public abstract void colision();

}
