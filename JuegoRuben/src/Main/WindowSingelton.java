package Main;

import Core.Field;
import Core.Window;

public class WindowSingelton {
	static Window w=null;
	
	private WindowSingelton(Field f) {
		w = new Window(f);
	}
	
	public static Window getWindowSingleton(Field f) {
		if(w == null) {
			new WindowSingelton(f);
		}else {
			w.changeField(f);
		}
		return w;
	}
	

}
