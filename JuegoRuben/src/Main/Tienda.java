package Main;

import java.util.ArrayList;

import Core.Sprite;
import Core.Window;

public class Tienda {

	static ArrayList<Sprite> sprites = new ArrayList<>();

	static Window w = WindowSingelton.getWindowSingleton(Main.f);

	static Pj MB;
	static Texto item1 = new Texto("texto", 270, 625, 270, 625, "item 1");
	static Texto item2 = new Texto("texto", 1490, 625, 1490, 625, "item 2");

	public static void main(String[] args) {
		MB = Pj.getPj("MeatBoy", 120, 590, 170, 640, "IMG/MeatBoyQuieto.png");
		Main.f.background = "IMG/tienda.png";

		item1.textColor = 0xFFFFFF;
		item2.textColor = 0xFFFFFF;

		Escalera.escala(Main.f);
		Items.getInstance(Main.f);
		Mapa.Tienda(Main.f);

		while (true) {

			String item1t = "Item de velocidad +4 ";
			String item2t = "Item de salto + 7";
			
			item1.path = item1t; 
			item2.path = item2t; 

			input();
			ArrayList<Sprite> sprites = new ArrayList<>();

			sprites.addAll(Mapa.nivelTienda);
			Escalera.escalandoT();
			sprites.add(item1);
			sprites.add(item2);
			gravity();

			if (Main.f.getCurrentMouseX() != -1) {
				System.out.println(Main.f.getMouseX() + " " + Main.f.getMouseY());
			}
			if (MB.collidesWithList(Mapa.puertaNivelTienda).size() > 0 || w.getKeysDown().contains('l')) {
				cleanUP();
				sprites.clear();
				Main.main(null);
				break;
			}

			Main.f.draw(sprites);

			try {
				Thread.sleep(35);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private static void cleanUP() {
		Mapa.nivelTienda.clear();
		Mapa.spTienda.clear();
		Mapa.plataformaTienda.clear();
	}

	/**
	 * Input
	 * 
	 * Detecta que tecla tienes presionada para moverte A= izquierda D = derecha W =
	 * saltar
	 * 
	 */
	private static void input() {
		if (w.getPressedKeys().contains('a')) {

			MB.move('a');
			MB.changeImage("IMG/PJtest.gif");
			if (MB.isOnColumn(Main.f)) {
				MB.getSided(Main.f);
			}

		}
		if (w.getPressedKeys().contains('d')) {

			MB.move('d');
			MB.changeImage("IMG/CaminarDerecha.gif");
			if (MB.isOnColumn(Main.f)) {
				MB.getSided(Main.f);
			}

		}
		if (w.getPressedKeys().contains('w')) {
			MB.jump();

		}
		if (!w.getPressedKeys().contains('d') && !w.getPressedKeys().contains('a')) {
			MB.changeImage("IMG/MeatBoyQuieto.png");

		}

		if (w.getPressedKeys().contains('d') && w.getPressedKeys().contains('a')) {
			MB.changeImage("IMG/MeatBoyQuieto.png");

		}

	}

	/**
	 * Gravity
	 * 
	 * le paso fall que es la funcion de saltar
	 * 
	 */

	private static void gravity() {
		MB.fall(Main.f);

	}

}
