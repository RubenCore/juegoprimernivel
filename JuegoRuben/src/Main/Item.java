package Main;

import Core.Sprite;

public class Item extends Sprite {

	public ItemType type;

	public Item(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		this.terrain = false;
		this.type = ItemType.SPEED;
	}
	
	public Item(String name, int x1, int y1, int x2, int y2, String path, ItemType type) {
		super(name, x1, y1, x2, y2, path);
		this.terrain = false;
		this.type = type;
	}
	
	
}
enum ItemType{
	SPEED, JUMP, COIN
}